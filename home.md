<div class="cwikmeta">  
{  
"title": "NextChain"  
} </div>

# NextChain


<img align="right" src="NextChainFlag.png" width=200></img>*A BCH Innovation Testnet* 

NextChain is a testnet for BCH innovation.  It is for experimentation with novel features that are or could become candidates for inclusion into Bitcoin Cash.


# Features

## Candidate Features
Features on this testnet exist in various states of maturity.  Although we encourage readers to read about all the ideas in NextChain, and contribute to their development, the following features are the best current candidates for BCH adoption, based on their maturity and BCH culture:

- Group Tokenzation
- [OP_PLACE](/op_place.md)
- Large Integer operations (*via [BigNum](/bignum.md)*)


## Feature Overview
This testnet currently implements the following features:

- [Block structure changes](/blockheader.md)
 - [Transaction structure changes](/transactionChanges.md)
 - [Addresses](/addresses.md)
- Large Integer operations (*via [BigNum](/bignum.md)*)
- Transaction state introspection (*via [OP_PUSH_TX_STATE](/op_push_tx_state.md)*)
- Script Covenants (*via [template scripts](/templatescripts.md)*)
- Miner enforced fungible tokens (*via [group tokens](/grouptokens.md)*)
- Miner enforced non-fungible tokens (*via [group tokens](/grouptokens.md)*)
- Merkleized Abstract Syntax Trees (MAST) (*via [OP_EXEC](/op_exec.md)*)
- Modified proof-of-work algorithm ([nextPOW](/nextpow.md))

[Forthcoming features](/future__features.md)

# Technology Overview
 [BigNum](/bignum.md), 
 [Buffers](/scriptbuffer.md), 
 [Counterparty and Protocol Discovery (CAPD) Service](/capd.md)
 [NextPOW](/nextpow.md), 
 [Groups](/grouptokens.md),
 [Templates](/templatescripts.md),
 [NextSigHash](/sighash.md),
 
## Script Opcodes

- [OP_BIGNUM2BIN](/op_bignum2bin.md)
- [OP_BIN2BIGNUM](/op_bin2bignum.md), 
- [OP_SETBMD](/op_setbmd.md)
- [OP_EXEC](/op_exec.md)
- [OP_PUSH_BUFFER](/op_push_buffer.md)
- [OP_PUSH_TX_STATE](/op_push_tx_state.md)
- [OP_PLACE](/op_place.md)

## Attribute Opcodes
-  [OP_TEMPLATE](/op_template.md)
-  [OP_GROUP](/op_group.md)


## Example Applications
### [BigNum](/bignum_applications.md)

# Concepts
Topics in this section are conceptual only at this point.

[Aggregated Client-side Bloom Filtering](/aggregate_bloom_filter.md)

[Transaction Aggregates](/transaction_aggregates.md)

[Introspection and checkdatasig as a generalized sigHash Replacement](/ramifications__of__introspection__and_cds.md)

[Groups Used as a Versatile, Stateful, Parallel Contract-like Blockchain Entity](/group__tokens__and__contracts.md)

# Connecting
 * A full node is located at: n1.nextchain.cash
 * A seeder will soon be up at: seed.nextchain.cash
 * P2P network
The default port for the P2P layer is 7228
The message start is 0x72, 0x27, 0x12, 0x21
 * RPC access
The default port for RPC access is 7227
* Electrum
The default electrum protocol port is 7229
* BCHD
The default gRRP port is 7226

## Explorer
Look to https://explorer.nextchain.cash to browse the blockchain.

# Software

* Full Node: [Bitcoin Unlimited](/software_bu_node.md)
* Miner: The Bitcoin Unlimited full node's "bitcoin-miner" program has been modified to support this chain.

# Engineering Topics

- [Satoshi Client Implementation Notes](/impl_topics.md)