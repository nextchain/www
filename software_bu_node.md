NextChain is implemented in Andrew Stone's fork of the Bitcoin Unlimited full node, on the "NextChain" branch:

## Source Access
```
git clone git@github.com:gandrewstone/BitcoinUnlimited.git
git checkout NextChain
```

## Building

Build proceeds as Bitcoin Unlimited is normally built (described [here](https://gitlab.com/bitcoinunlimited/BCHUnlimited/-/blob/dev/README.md)), including its prerequisites, with the following changes:

* Install GNU multiprecision library (libgmp)

On debian/ubuntu:
```bash
sudo apt-get install libgmp-dev
```

* Build libbitcoincash.so during the bitcoind build (optional, recommended)
```bash
./configure --enable-shared
make -j20
```

* Build electrscash (optional, recommended)
Electrscash presents the Electrum wallet protocol to light wallets and blockchain explorers.  If you will not use light wallets, you do not need to build this.  If you have never built electrscash, you must first install various dependencies, etc described [here](https://github.com/BitcoinUnlimited/ElectrsCash/blob/master/doc/usage.md#building-binaries))
```bash
make electrscash
```

*Similar to Bitcoin Unlimited's release branch, if you did an out-of-source build, you will need to symlink the electrscash binary to your out-of-source location.*

## Configuration
Blockchain data is stored in the "~/.xnex" directory on Linux, 

In .xnex/bitcoin.conf set:
```bash
# Access the NextChain blockchain (this is the default)
nextchain=1
# (optional) Enable transaction indexing
txindex=1
# Enable light client support (if you built electrscash)
electrum=1
electrum.host=0.0.0.0
# Ensure connectivity to the rest of the network
addnode=n1.nextchain.cash
```

## Running

Run ./bitcoind to start the full node.
Run ./bitcoin-miner -cpus=XX to start mining.