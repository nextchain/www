<div class="cwikmeta">    
{   
"title": "Future Features",
"related": ["/capd.md"]
} </div>

# Future Features

## Counterparty And Protocol Discovery (CAPD) Service
A transient decentralized anonymous content-addressable messaging service that allows participants to to discover transaction partners and execute protocols with them.  Proof of work is used to discourage spam.

[Document](/capd.md)

## Large scripts 
(*via OP_PUSH_BUFFER, [OP_EXEC](op_exec.md), and OP_TEMPLATE*)  

## Script libraries (via groups)

TBD

## Object Property Commitments  

Relay objects across the network in a manner that discovers and rejects underfee transactions (see relevant doc for details).  
  
## Transaction format changes

Add prevout amount (how many satoshis an input is bringing into a transaction) in the transaction input.  If it is the wrong amount, the transaction will not confirm.

This helps entities using transactions (for example who need to fund a tx), it helps prevent accidental fee overpay, and it will help a transition to a hybrid account/utxo model.

## Sighash algorithm expansion:   

SIGHASH_0THRU: signs all outputs from 0 to N (exclusive to SIGHASH_ALL and SINGLE)  
SIGHASH_OFFSET: indicates an extra byte to the sighash (preceding it since sighash is at the end).  This byte is interpreted as a signed 8-bit integer.  To get N add this byte to the input index. (exclusive to SIGHASH_ALL)  
  

New modes:   
SIGHASH_SINGLE and SIGHASH_OFFSET: output[input index + N] is signed.  Without SIGHASH_OFFSET current behavior is output[input index] is signed.  
  

SIGHASH_0THRU: as described above  
  

SIGHASH_0THRU and SIGHASH_OFFSET: outputs[0..input index + N] are signed. If input index + N < 0 reject.  
  

Note that it may also be possible to implement this functionality with tx introspection and checkdatasig.  
  

## Miner fee policy commitment in blocks  
  
TBD  

## Reuse block hash leading 0 bits  
  

As block height:  (POWALG(block) & ~(0xffffffff << 256-4) < difficulty target) && (POWALG(block) >> 256-4) == height)