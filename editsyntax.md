<div class="cwikmeta" style="display:none">
{
"title": "CWIK"
} </div>

## Overview

*Cwik (pronounced 'quick') is an editable documentation web site (wiki) that uses git and markdown to make an easy to manipulate and edit pages.*

 - All pages are stored in an enhanced Markdown format
 - All data is stored in a source control system (git)

## Enhanced Markdown

Here are some examples. "Edit" this document to see the underlying implementatons.

### Basic Markdown
*italics* ```*italics*```
**bold** ```***bold***```

```bash
Code blocks use 3 backtics, with a language specifier, like this:
```
<pre>
```bash
3 backtics open and close code blocks.
```
</pre>

```---``` is a horizontal rule

---

Tables are made with bars and dashes
|  head0 | head1 |
|--------|-------|
| cell 0,0 | cell 0,1 |
like this:
```
|  head0   |  head1   |
|----------|----------|
| cell 0,0 | cell 0,1 |
```
aligning columns is optional.


### Images

You can upload an image (if you are logged in with edit priviledges) by browsing to the non-existent image's URL.
Try it here: [this image doesn't exist](non_existent_image_file.svg).
Or change an uploaded image by browsing to the image's URL with "?upload=1".
For example [/BitcoinCashCoin.svg?upload=1](/BitcoinCashCoin.svg?upload=1).


 ![Markdown image](/BitcoinCashCoin.svg)  ![sized](/BitcoinCashCoin.svg =100x)
```
![Markdown image](/BitcoinCashCoin.svg)  ![sized](/BitcoinCashCoin.svg =100x)
```
Flowing text alongside an image is possible, but requires HTML.  <img align="right" src="/coins1.png" width="200px"></img>
Like this:
```html
<img align="right" src="/coins1.png" width="200px"></img>
```

You may not see the image in the editor, if relative URLs are used.

<img align="left" src="https://www.bitcoinunlimited.info/img/slideshow/blockchain-dim.png" width="400px"></img>
So you will see the left image to the left of this text in the editor because it is a fully specified URL, like this:
<br>
```html
<img align="left" src="https://www.bitcoinunlimited.info/img/slideshow/blockchain-dim.png" width="400px"></img>
```
You can stop parallel flow with this html ```<br class="clear">```
<br class="clear">

---
### Math
[Full Documentation](https://katex.org/docs/supported.html)
The *Gamma function* satisfying $\Gamma(n) = (n-1)!\quad\forall n\in\mathbb N$ is via the Euler integral
$$
\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.
$$

### Flowchart
[Full Documentation](https://mermaidjs.github.io/#/flowchart)
```mermaid
graph LR
A[Square Rectangle] -- paren type controls shape --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
```

```mermaid
graph BT
A["bottom"] == Link text ==> B[TOP]
style B fill:#906,stroke:#333,stroke-width:2px;
style A fill:#ff6,stroke:#333,stroke-width:8px;
```
```mermaid
graph RL
    %% Comments after double percent signs
    A(("(){}[]"))-->B>";place special chars in quotes"]
```

### Protocol (Sequence) diagram
[Full Documentation](https://mermaidjs.github.io/#/sequenceDiagram)
```mermaid
sequenceDiagram
  Alice ->> Bob: Hello Bob, how are you?
  Bob-->>John: How about you John?
  Bob--x Alice: I am good thanks!
  Bob-x John: I am good thanks!
  Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

  Bob-->Alice: Checking with John...
  Alice->John: Yes... John, how are you?
```

### Gantt Chart

```mermaid
gantt
dateFormat  YYYY-MM-DD
title Example Chart
excludes weekdays 2020-01-01

section A section
Completed            :done,    des1, 2020-01-01,2020-02-01
Active               :active,  des2, 2020-02-01, 20d
Next                 :         des3, after des2, 10d
Next2                :         des4, after des3, 5d
```

### UML Diagram

```mermaid
classDiagram
ScriptImportedState *-- BaseSignatureChecker
ScriptMachine *-- Stack: main and altstack
ScriptMachine *-- ScriptMachineResourceTracker
ScriptMachine *-- ScriptImportedState
BaseSignatureChecker <|-- TransactionSignatureChecker
BaseSignatureChecker <|-- DummySignatureChecker

ScriptMachine : BigNum bitNumModulo
ScriptMachine : eval()
ScriptMachine : step()

ScriptImportedState : CTransactionRef tx
ScriptImportedState : int nIn [input idx]
ScriptImportedState : amount [input coin quantity]
```

## Sharing


### Sharing

CWIK supports the [open graph protocol](https://ogp.me/), which is how social media site display nice page links.  By default, the first "#" or "h1" tag becomes the page title, the first italics (*something* or "em" tag) becomes the summary, and the first image becomes the summary image.

It is also possible to override these settings with a special page header, in JSON format.
```
<div class="cwikmeta"> 
{  
'title': 'CWIK',
'related': ['local','related','pages'],
'summary': 'one or two sentence summary',
'pic': 'full URL of picture, not .svg format!'
} </div>
```

## Git storage

Your changes accumulate in local storage on the server.  Press the "commit" button (upper right) to commit all edits.

While many tasks can be done within the CWIK web site, the power of git is not meant to be ignored.  Users and administrators are encouraged to use git to deal complex merges, etc.

Look here: https://gitlab.com/nextchain/www to see the backing repository.

## Future
 - Edits are signed by cryptocurrency tokens
 - Integrated with source code documentation generators
 - Discussion and Commentary
 
## Token Integration
**To be implemented**

## Source Documentation Generation
**To be implemented**

This system is able to integrate multiple Git-hosted repositories.  A typical configuration would be one documentation focused repository and multiple source repositories.  These source repositories can provide inline documentation that is extracted via various documentation generators (subsequently called "codedoc") and provide access to "raw" source code.

Linking between documentation and code/codedoc or codedoc to codedoc occurs via a special syntax that allows references to code blocks, is tolerant of some code reorganizations, and identifies and records broken links for human repair.

There is a processing step to convert these links to .md format.  So the full source data preparation steps are:

```mermaid
graph TB
A["git source code"] == doc generator ==> B["code doc"]
D["git doc files"] ==> E(("Link resolver"))
A ==> E
B ==> E
E ==> F["Markdown files"]
E ==> G["broken link report<br/>(markdown file)"]
style A fill:#ff6,stroke:#333,stroke-width:8px;
style D fill:#ff6,stroke:#333,stroke-width:8px;
style F fill:#3A3,stroke:#334,stroke-width:4px;
style G fill:#3A3,stroke:#334,stroke-width:4px;
```