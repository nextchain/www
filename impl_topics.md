<div class="cwikmeta">  
{  
"title": "Implementation Topics"  
} </div>


# Script Machine

The interpreter is easily encapsulated into a class.  This keeps the script interpreter isolated and allows multiple simultaneous instances.  This change just makes sense and was made in Bitcoin Unlimited a long time ago (independently of NextChain work).

## Script Imported State

Scripts can now access some "external" state through the PUSH_TX_STATE opcode.  The script virtual machine needs to be very deliberate about exactly what external state is accessible, focusing on efficiency and correctness.  In particular, a code path that currently validates the script must not in the future become invalid.  This eliminates importing the current block hash, for example (however, note that it would be possible to import the hash of the Nth block).

By placing all required state in a const serializable object, scripts can be executed in parallel, on hardware, in remote machines, etc.

The "BaseSignatureChecker" object used to supply this information since the only external data was the sighash used during transaction signature checks.  This object subsequently saw feature creep, as two functions unrelated to signatures were added: CheckLockTime, and CheckSequence.

Rather than continue the feature creep, the SignatureChecker was replaced with a class called ScriptImportedState that contains all information available to a script: the SignatureChecker, the current transaction, the input amount, the index of this input, and prevout information from the UTXO set.

It is optional to supply this information.  If a script accesses information that is not available, it fails with the "SCRIPT_ERR_DATA_REQUIRED" error.