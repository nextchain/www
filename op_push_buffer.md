<div class="cwikmeta">  
{  
"title": "OP_PUSH_BUFFER",
"related":["scriptbuffer.md"]  
} </div>

# OP_PUSH_BUFFER
*Pushes a reference to a buffer of data onto the stack*

## Syntax and Stack
 **OP_PUSH_BUFFER2** *length* *data* => *bRef*<sup>*[?](opcodeSyntax.md)*</sup>
 **OP_PUSH_BUFFER4** *length* *data* => *bRef*<sup>*[?](opcodeSyntax.md)*</sup>

- *length*: 2 or 4 byte little endian length of data
- *data*: *length* bytes of data

Returns:

- *bRef*: a reference (see [here](scriptbuffer.md)) to the provided data.

#### Binary representation
OP_PUSH_BUFFER2 is defined as the single byte 0xe8.
OP_PUSH_BUFFER4 is defined as the single byte 0xe9.

## Operation

A reference to *data* is placed upon the stack.  This reference is not an object or number that can be manipulated or indexed, except via OP_SPLIT and OP_EXEC.

## Limits

**T.L1** The max buffer size is limited by the maximum transaction size, and by the the transaction fee an issuer is willing to pay.

## Implementation notes
It is possible for the buffer reference to point to the memory in the transaction itself, making this opcode consume less stack memory than a typical stack data push.


## Design considerations
It is essential to allow scripts or script fragments to be larger than the 520 byte maximum stack item length.  But limiting the stack item length is an important technique to mitigate attacks that attempt to consume excessive CPU, and will make FPGA or ASIC script machines much simpler.   Such machines can more efficiently use an extra byte to identify the type of a stack item than allow variable sized stack items.

At the same time, the P2SH, MAST, and script template transaction output format have clear advantages over normal output scripts, so making the output script the only possible large script is not viable.