<div class="cwikmeta">    
{   
"title": "OP_GROUP"   
} </div>  
  

# OP_GROUP

*Script attribute that associates a transaction output with a group*

## Syntax and Stack  

*Gid*  *Gqty* **OP_GROUP** => (nothing) <sup>*[?](opcodeSyntax.md)*</sup>
- *Gid*: The group identifier.  Since OP_GROUP is an attribute, this MUST be a Literal PUSHDATA()
- *Gqty*: The group quantity.  Since OP_GROUP is an attribute, this MUST be a Literal PUSHDATA() 

### Binary Representation
OP_GROUP is represented by the single byte 0xee.

## Operation

### Template scripts
Since OP_GROUP is an attribute, it MUST never appear in code.  If it does appear, the script fails with an invalid opcode error.

### Legacy scripts
The OP_GROUP clause MAY appear as the first element of P2PKH and P2SH scripts only.  In this case this clause MUST be skipped.  Execution should begin after the OP_GROUP opcode.  If it subsequently appears, the script fails with an invalid opcode error.

### Consensus operation

The effect of groups on consensus operation is specified [here](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit?usp=sharing).