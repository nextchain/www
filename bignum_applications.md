# BigNum applications

## Importing foreign crypto systems

With BigNum numbers it is possible to implement signature verifications systems that are not natively supported by Bitcoin Cash.  This would allow money to be constrained by identities defined in other crypto systems.
