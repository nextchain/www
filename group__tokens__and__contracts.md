<div class="cwikmeta">    
{   
"title": "Group Tokenization as Contracts",   
"related":["/op_exec.md", "/op_push_tx_state.md"]   
} </div>    

# Group Tokenization as Contracts 

*The UTXO architecture creates interesting limitations on stateful blockchain entities that can be overcome using Group Tokenization*

## Problem Statement

