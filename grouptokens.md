<div class="cwikmeta">  
{  
"title": "Groups",
"related": ["templatescripts.md"]
} </div>

# Groups

## Overview

"Groups" (or "Group Tokenization") is a technique that allows additional consensus rules, limited by the expressibility of the BCH Script language, to be created by anyone and then be enforced by miners upon a subset of UTXOs and their descendants. 

These UTXOs can have a balance of BCH tokens and/or a balance of a new token type that is implicitly created along with the group.  Since one key use of Groups is to create these tokens, the name "Group Tokenization" is commonly used to describe this technology.

Generally, BCH or tokens "within" a Group are conserved in a transaction -- special permissions (called "group authorities") are required to allow BCH to enter or leave or to allow tokens to be created or destroyed.

A Group can enforce script template consistency -- in other words, the output script templates must be the same as the input script templates.  In this case, a special group authority can be used to introduce a new script template into the Group.  However, permission-to-spend a UTXO must still be used to actually constrain UTXOs into the new script template.  In this manner, Groups can be created with "upgradable" constraint scripts.  However, a holder's coins cannot be "upgraded" without their consent, unless the current constraints allow such.


## Literature

A specific technical specification and proposal for inclusion into Bitcoin Cash is located here:

https://www.bitcoinunlimited.net/grouptokenization/groupbchspec

Nextchain Group Tokens implements the above proposal.  The Nextchain blockchain has also implemented separate but interacting features such as OP_EXEC, Script Templates, and transaction introspection (OP_PUSH_TX_DATA).

Although implementable in a small amount of consensus code, Groups enable awesome features especially when combined with transaction introspection opcodes.  These featureas are beyond the scope of this document.  For more information, a larger investigation of the technology is located here:

https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit?usp=sharing

## Example

A basic example the creates a group using the wallet associated with the full node, then mints, transfers and melts tokens is available [here](/groupTokensCliExample.md).