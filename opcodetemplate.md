<div class="cwikmeta">  
{  
"title": "OP_CODE Template",
"related": ["home.md","editsyntax.md"]
} </div>

# OP_{NAME}
*Italic one sentence description*

## Syntax and Stack
*param1*...*paramN* **OP_{NAME}** => ret1..retN<sup>*[?](opcodeSyntax.md)*</sup>

- *param1...paramN*: Describe each parameter
- *ret1...retN*: Describe each return

#### Binary representation
OP_NAME is defined as the single byte {specify byte}.

## Operation

Specify the operation of this opcode in complete and painstaking detail, marking every operational requirement with a bold test identifier, for example **T.O1**.

## Limits

**T.L1**: Specify constraints with bold test identifiers

## Implementation notes

Describe interesting ways implementation was or may be achieved.

## Design considerations

Describe the reasoning behind the choices made for this opcode verses other possibilities.

