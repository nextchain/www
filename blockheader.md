# Nextchain Block Structure

## Block header

### Fields

#### Mini header
The miniheader forms the minimum data required to form a blockchain and calculate chain work.  It (the hash of the extended header, and the nonce) is the minimum a light client might store if no transactions interesting to it exist in the block.
 
uint256 hashPrevBlock:  This the hash of the parent block.  As specified in the Satoshi-header.
uint32_t nBits: This is the target required to solve this block.

#### Extended header
The extended header provides information about this block and its location in the blockchain.

uint256 hashAncestor: hash of a specific ancestor block
uint256 hashMerkleRoot: block's transactions' merkle root
uint256 hashTxFilter: commitment to a probabilistic transaction/address filter that allows light clients to discover if they may  need the block. (e.g. neutrino)
uint32_t nTime: block creation time in seconds since the epoch
uint64_t height: block height
uint256 chainWork: Cumulative work in the chain
uint64_t size: Block size in bytes, not counting the nonce
uint64_t txCount: Number of transactions in the block
uint64_t maxSize:   Maximum allowed block size in bytes of a block at this height
uint64_t feePoolAmt: quantity of satoshis in fee pool AFTER transaction evaluation (algorithmically determined).
vector[0-128] bytes utxoCommitment: A commitment to the currently unspent coins
vector[] minerData:  TBD
vector[0-16] bytes nonce: data required to solve the block POW

##### Extended header field definitions
**uint256 hashAncestor**: This is the hash of an ancestor block.  It is used to quickly skip backwards in the blockchain, allowing one to efficiently prove that a block has a particular ancestor.  This field MUST contain the hash of the ancestor block a height $H_a$ where 

$$
H_a = \begin{cases}
H/2, if H\&1 == 1 \\
H\& \text{bitwiseNot(lowest1bit(H)}, \text{otherwise}
\end{cases}
$$

In English, if the height is odd, find the ancestor at half the height (todo, make this more useful).  Otherwise, set the lowest 1 bit in the height to 0 and find that ancestor.  

When traversing, the even heights allow code to take larger and larger jumps, resulting in about $2log_2(n)$ steps to find an ancestor.  If the code would jump beyond the target ancestor, it can use the hashPrevBlock field to move back a single step.  If the odd heights followed this algorithm, they would be redundant with hashPrevBlock, so they function as a big step backwards.
