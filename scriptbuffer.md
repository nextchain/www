<div class="cwikmeta">  
{  
"title": "Script Buffers"  
} </div>

# Script Buffers
*Allow script access to large quantities of data*

## Problem Statement
Script PUSH operations are limited to the maximum size of a stack item.  This is currently 520 bytes.  While this limit was introduced for good reasons, namely to prevent exploitative scripts from using excessive amounts of memory, it severely limits the ability of scripts to access and manipulate data.

Most importantly, since the introduction of P2SH, scripts themselves are pushed onto the stack.  This means that scripts that are larger than 520 bytes are not available to anyone except miners (since there are no "standard" scripts that are > 520 bytes).

## Operation

A new type is introduced, called a buffer.  A buffer exists as a single entity, and stack items only contain references to the buffer.

Stack manipulation operations copy and move the buffer reference rather than the buffer itself.

[OP_PUSHBUF](op_push_buffer.md) places a reference to a buffer onto the stack.

**T.O1**:  OP_SPLIT **MUST** accept a buffer as the object it is splitting.  The buffer reference is removed from the stack and 2 normal stack items are pushed.  This allows sections of the buffer to be extracted and pushed onto the stack as a normal stack item.  If the left or right side of the split is larger than the maximum stack item length, the buffer is truncated to the maximum stack length, at the beginning or the end respectively.  This allows two OP_SPLIT operations (and potentially and OP_SWAP and OP_DROP to clean up the stack) to select a specific range of the buffer.

**T.O2**:  OP_EXEC **MUST** accept a buffer as the code it is executing.

**T.O3**: all stack operations **MUST** accept buffer arguments and will move or copy the buffer reference as appropriate.

**T.O4** All other opcodes **MUST NOT** accept buffers.
String modification operations (e.g. OP_CAT, OP_REVERSEBYTES) on buffer references are illegal.  Numerical operations on buffer references are illegal.  Using a buffer reference for an opcode that wants a number is illegal.

**T.O5** A script that completes with a buffer reference as the top item on the stack evaluates to *FALSE*

Scripts **SHOULD NOT** deliberately fail via an invalid operation on a buffer.  If opcodes are subsequently enabled to accept buffers as arguments, these scripts will change behavior.

## Limits
The size of the data pushed is limited by the maximum size of a transaction.  Since no copies of the data need be made (see the implementation notes), no additional restrictions need be applied.

## Implementation Notes

A memory efficient implementation could point buffer references directly to the data in the script.